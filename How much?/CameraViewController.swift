//
//  CropViewController.swift
//  TheFineDetails
//
//  Created by Dave Dombrowski on 2/10/16.
//  Copyright © 2016 justDFD. All rights reserved.
//

import UIKit
import AVFoundation
import TesseractOCR
import Appodeal
//import SwiftOCR

func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

func / (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x / scalar, y: point.y / scalar)
}

class CameraViewController: UIViewController, UINavigationControllerDelegate,AVCapturePhotoCaptureDelegate, UIImagePickerControllerDelegate {
    
    //@IBOutlet
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var imgOverlay: UIImageView!
    @IBOutlet weak var myImageView: CropImageView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var resultImage: UIImageView!
    @IBOutlet weak var recognizedLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var rateValue:Double = 1.00
    
    var scaledImageFrame = CGRect.zero
    
    var touchpointTL = TouchpointView(point: CGPoint.zero, text:"TL")
    var touchpointTR = TouchpointView(point: CGPoint.zero, text:"TR")
    var touchpointBL = TouchpointView(point: CGPoint.zero, text:"BL")
    var touchpointBR = TouchpointView(point: CGPoint.zero, text:"BR")
    
    var cgTL = [CGPoint]()
    var cgTR = [CGPoint]()
    var cgBR = [CGPoint]()
    var cgBL = [CGPoint]()
    
    var tpTL = [CIVector]()
    var tpTR = [CIVector]()
    var tpBR = [CIVector]()
    var tpBL = [CIVector]()

    var ciInput:CIImage!
    var cgImage:CGImage!
    var ctx:CIContext!
    
    var thumbnails = UIImage()
    
    var captureSession = AVCaptureSession();
    var sessionOutput = AVCapturePhotoOutput();
    var sessionOutputSetting = AVCapturePhotoSettings(format: [AVVideoCodecKey:AVVideoCodecJPEG]);
    var previewLayer = AVCaptureVideoPreviewLayer();
    var croppingEnabled: Bool = true
    var libraryEnabled: Bool = false
    
    func getScaledImageFrame() -> CGRect {
        var imageFrame = CGRect.zero
        if myImageView.scaledSize.height == myImageView.frame.height {
            imageFrame = CGRect(x: (myImageView.frame.width-myImageView.scaledSize.width)/2, y: 0, width: myImageView.scaledSize.width, height: myImageView.scaledSize.height)
        } else {
            imageFrame = CGRect(x: 0, y: (myImageView.frame.height-myImageView.scaledSize.height)/2, width: myImageView.scaledSize.width, height: myImageView.scaledSize.height)
        }
        return imageFrame
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    func createDifferentialArray(_ startPoint:CGPoint, endPoint:CGPoint) ->[CGPoint] {
        let diffPoint = endPoint-startPoint
        let diffX = diffPoint.x/4
        let diffY = diffPoint.y/4
        var cgArray = [CGPoint]()
        cgArray.append(startPoint)
        cgArray.append(CGPoint(x: cgArray[0].x+diffX, y: cgArray[0].y+diffY))
        cgArray.append(CGPoint(x: cgArray[1].x+diffX, y: cgArray[1].y+diffY))
        cgArray.append(CGPoint(x: cgArray[2].x+diffX, y: cgArray[2].y+diffY))
        cgArray.append(CGPoint(x: cgArray[3].x+diffX, y: cgArray[3].y+diffY))
        return cgArray
    }
    
    func crop() -> Void {
        if scaledImageFrame == CGRect.zero {
            scaledImageFrame = getScaledImageFrame()
        }
        
        cgTL = createDifferentialArray(scaledImageFrame.origin, endPoint: touchpointTL.center)
        cgTR = createDifferentialArray(scaledImageFrame.origin+CGPoint(x: scaledImageFrame.width, y: 0), endPoint: touchpointTR.center)
        
        cgBL = createDifferentialArray(scaledImageFrame.origin+CGPoint(x: 0, y: scaledImageFrame.height), endPoint: touchpointBL.center)
        cgBR = createDifferentialArray(scaledImageFrame.origin+CGPoint(x: scaledImageFrame.width, y: scaledImageFrame.height), endPoint: touchpointBR.center)
        
        tpTL.append(createVector(createScaledPoint(cgTL[0])))
        tpTR.append(createVector(createScaledPoint(cgTR[0])))
        tpBR.append(createVector(createScaledPoint(cgBR[0])))
        tpBL.append(createVector(createScaledPoint(cgBL[0])))
        
        tpTL.append(createVector(createScaledPoint(cgTL[1])))
        tpTR.append(createVector(createScaledPoint(cgTR[1])))
        tpBR.append(createVector(createScaledPoint(cgBR[1])))
        tpBL.append(createVector(createScaledPoint(cgBL[1])))
        
        tpTL.append(createVector(createScaledPoint(cgTL[2])))
        tpTR.append(createVector(createScaledPoint(cgTR[2])))
        tpBR.append(createVector(createScaledPoint(cgBR[2])))
        tpBL.append(createVector(createScaledPoint(cgBL[2])))
        
        tpTL.append(createVector(createScaledPoint(cgTL[3])))
        tpTR.append(createVector(createScaledPoint(cgTR[3])))
        tpBR.append(createVector(createScaledPoint(cgBR[3])))
        tpBL.append(createVector(createScaledPoint(cgBL[3])))
        
        tpTL.append(createVector(createScaledPoint(cgTL[4])))
        tpTR.append(createVector(createScaledPoint(cgTR[4])))
        tpBR.append(createVector(createScaledPoint(cgBR[4])))
        tpBL.append(createVector(createScaledPoint(cgBL[4])))
        
        thumbnails = doPerspectiveCorrection(ciInput,
                                             context: ctx,
                                             topLeft: tpTL[4],
                                             topRight: tpTR[4],
                                             bottomRight: tpBR[4],
                                             bottomLeft: tpBL[4])
        performImageRecognition(image: thumbnails)
        /*swiftOCRInstance.recognize(thumbnails) {recognizedString in
            DispatchQueue.main.async(execute: {
                self.recognizedLabel.isHidden = false
                self.recognizedLabel.text = recognizedString
                self.activityIndicator.stopAnimating()
            })
        }*/
    }
    
    func performImageRecognition(image: UIImage) {
        tesseract(image: image)
        //swiftOCR(image: image)
        
    }
    
    func tesseract(image: UIImage) -> Void {
        // 1
        let tesseract = G8Tesseract()!
         // 3
         tesseract.language = "eng"
         
         tesseract.engineMode = .tesseractOnly
         // 4
         tesseract.pageSegmentationMode = .auto
         // 5
         tesseract.maximumRecognitionTime = 60.0
         // 6
         tesseract.image = image.g8_blackAndWhite()
         tesseract.recognize()
         // 7
         self.recognizedLabel.isHidden = false
         self.activityIndicator.stopAnimating()
         print(Calendar.current)
         if var string = tesseract.recognizedText {
         string = (string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789.").inverted) as NSArray).componentsJoined(by: "")
         print("recognizedLabel: \(string)")
         if let double = Double(string) {
         let doubleX = double * self.rateValue
            
         self.recognizedLabel.text = String.init(format: "%.2f", doubleX)
         print(Calendar.current)
         } else {
         /*let alert: UIAlertController  = UIAlertController(title: "Status", message: "Its not a number", preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
         self.present(alert, animated: true, completion: nil)*/
         }
         
         }
    }
    
    /*func swiftOCR(image: UIImage) -> Void {
        let ocrInstance = SwiftOCR()
        print(Calendar.current)
        ocrInstance.recognize(image) { recognizedString in
            DispatchQueue.main.async(execute: {
                var string = recognizedString
                string = (string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789.").inverted) as NSArray).componentsJoined(by: "")
                print("recognizedLabel: \(string)")
                if let double = Double(string) {
                    let doubleX = double * self.rateValue
                    self.recognizedLabel.text = String(describing: doubleX)
                    print(Calendar.current)
                } else {
                    /*let alert: UIAlertController  = UIAlertController(title: "Status", message: "Its not a number", preferredStyle: .alert)
                     alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                     self.present(alert, animated: true, completion: nil)*/
                }
            })
        }
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myImageView.addSubview(touchpointTL)
        myImageView.addSubview(touchpointTR)
        myImageView.addSubview(touchpointBL)
        myImageView.addSubview(touchpointBR)
        ctx = CIContext(options: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUIBar()
        
        let deviceDiscoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [AVCaptureDeviceType.builtInDuoCamera, AVCaptureDeviceType.builtInTelephotoCamera,AVCaptureDeviceType.builtInWideAngleCamera], mediaType: AVMediaTypeVideo, position: AVCaptureDevicePosition.back)
        for device in (deviceDiscoverySession?.devices)! {
            if(device.position == AVCaptureDevicePosition.back){
                do{
                    let input = try AVCaptureDeviceInput(device: device)
                    if(captureSession.canAddInput(input)){
                        captureSession.addInput(input);
                        
                        if(captureSession.canAddOutput(sessionOutput)){
                            captureSession.addOutput(sessionOutput);
                            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
                            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                            previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.portrait;
                            cameraView.layer.addSublayer(previewLayer);
                            
                            if captureSession.isRunning == false {
                                captureSession.startRunning()
                            }
                        }
                    }
                    if captureSession.canAddOutput(sessionOutput) {
                        captureSession.addOutput(sessionOutput)
                    }
                }
                catch{
                    print("exception!");
                }
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.scaledImageFrame = CGRect.zero
        
        /*repeat {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                self.performCapture(UIButton())
            })
        } while true*/
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        previewLayer.frame = cameraView.bounds
    }
    
    func setupUIBar() -> Void {
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.frame = CGRect(x: 0.0, y: 4.0, width: 15.0, height: 30.0)
        backButton.setBackgroundImage(UIImage(named: "ic_back"), for: UIControlState.normal)
        backButton.addTarget(self, action: #selector(CameraViewController.back(sender:)), for: UIControlEvents.touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
    }
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func performCapture(_ sender: UIButton) {
        if (sessionOutput.connection(withMediaType: AVMediaTypeVideo)) != nil {
            activityIndicator.isHidden = false
            self.recognizedLabel.isHidden = true
            activityIndicator.startAnimating()
            let uniqueSettings = AVCapturePhotoSettings.init(from: self.sessionOutputSetting)
            sessionOutput.capturePhoto(with: uniqueSettings, delegate: self)
        }
    }
    
    
    @IBAction func performFlash(_ sender: UIButton) {
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureTorchMode.on) {
                    device?.torchMode = AVCaptureTorchMode.off
                    self.flashButton.setBackgroundImage(UIImage(named: "ic_flash_off"), for: UIControlState.normal)
                } else {
                    do {
                        try device?.setTorchModeOnWithLevel(1.0)
                    } catch {
                        print(error)
                    }
                    self.flashButton.setBackgroundImage(UIImage(named: "ic_flash_on"), for: UIControlState.normal)
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        let imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer!, previewPhotoSampleBuffer: previewPhotoSampleBuffer)
        self.myImageView.image = UIImage(data: imageData!)
        ciInput = CIImage(data: imageData!)?.applyingOrientation(imageOrientationToTiffOrientation(value: (UIImage(data: imageData!)!.imageOrientation)))
        self.showTouchpoints(true)
        self.crop()
    }
    
    func imageOrientationToTiffOrientation(value: UIImageOrientation) -> Int32
    {
        switch (value)
        {
        case .up:
            return 1
        case .down:
            return 3
        case .left:
            return 8
        case .right:
            return 6
        case .upMirrored:
            return 2
        case .downMirrored:
            return 4
        case .leftMirrored:
            return 5
        case .rightMirrored:
            return 7
        }
    }

    
    func doPerspectiveCorrection(
        _ image:CIImage,
        context:CIContext,
        topLeft:AnyObject,
        topRight:AnyObject,
        bottomRight:AnyObject,
        bottomLeft:AnyObject)
        -> UIImage {
            let filter = CIFilter(name: "CIPerspectiveCorrection")
            filter?.setValue(topLeft, forKey: "inputTopLeft")
            filter?.setValue(topRight, forKey: "inputTopRight")
            filter?.setValue(bottomRight, forKey: "inputBottomRight")
            filter?.setValue(bottomLeft, forKey: "inputBottomLeft")
            filter!.setValue(image, forKey: kCIInputImageKey)
            cgImage = context.createCGImage((filter?.outputImage)!, from: (filter?.outputImage!.extent)!)
            return UIImage(cgImage: cgImage)
    }
    
    func showTouchpoints(_ reload:Bool) {
        if scaledImageFrame == CGRect.zero {
            scaledImageFrame = getScaledImageFrame()
        }
        if reload {
            touchpointTL.frame = CGRect(origin: CGPoint(x: self.imgOverlay.frame.origin.x - 22, y: self.imgOverlay.frame.origin.y - 22), size: CGSize(width: 44.0, height: 44.0))
            
            touchpointTR.frame = CGRect(origin: CGPoint(x:self.imgOverlay.frame.origin.x  + self.imgOverlay.frame.size.width - 22,y: self.imgOverlay.frame.origin.y - 22), size: CGSize(width: 44.0, height: 44.0))
            touchpointBL.frame = CGRect(origin: CGPoint(x: self.imgOverlay.frame.origin.x - 22,y: self.imgOverlay.frame.origin.y + self.imgOverlay.frame.size.height - 22), size: CGSize(width: 44.0, height: 44.0))
            touchpointBR.frame = CGRect(origin: CGPoint(x: self.imgOverlay.frame.origin.x  + self.imgOverlay.frame.size.width - 22,y: self.imgOverlay.frame.origin.y + self.imgOverlay.frame.size.height - 22), size: CGSize(width: 44.0, height: 44.0))
            
        }
        touchpointBL.isHidden = false
        touchpointBR.isHidden = false
        touchpointTL.isHidden = false
        touchpointTR.isHidden = false
    }
    
    func createScaledPoint(_ pt:CGPoint) -> CGPoint {
        if scaledImageFrame == CGRect.zero {
            scaledImageFrame = getScaledImageFrame()
        }
        let x = (pt.x / myImageView.frame.width) * ciInput.extent.width
        let y = (pt.y / myImageView.frame.height) * ciInput.extent.height
        return CGPoint(x: x, y: y)
    }
    func createVector(_ point:CGPoint) -> CIVector {
        return CIVector(x: point.x, y: ciInput.extent.height - point.y)
    }
    func createPoint(_ vector:CGPoint) -> CGPoint {
        return CGPoint(x: vector.x, y: ciInput.extent.height - vector.y)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension CharacterSet {
    func allCharacters() -> [Character] {
        var result: [Character] = []
        for plane: UInt8 in 0...16 where self.hasMember(inPlane: plane) {
            for unicode in UInt32(plane) << 16 ..< UInt32(plane + 1) << 16 {
                if let uniChar = UnicodeScalar(unicode), self.contains(uniChar) {
                    result.append(Character(uniChar))
                }
            }
        }
        return result
    }
}
