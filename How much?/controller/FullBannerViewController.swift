//
//  FullBannerViewController.swift
//  How much?
//
//  Created by Marcus Ataide on 16/11/16.
//  Copyright © 2016 Math. All rights reserved.
//

import UIKit
import Appodeal

class FullBannerViewController: UIViewController, AppodealInterstitialDelegate {

    override func viewDidLoad() {
        Appodeal.setInterstitialDelegate(self)
        Appodeal.showAd(AppodealShowStyle.interstitial, rootViewController: self)
        bannerCheck()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Appodeal.setInterstitialDelegate(nil)
    }
    
    func bannerCheck() -> Void {
        let when = DispatchTime.now() + 5 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            self.performSegue(withIdentifier: "segueMain", sender: nil)
        }
    }
    
    func interstitialDidDismiss() {
        self.performSegue(withIdentifier: "segueMain", sender: nil)
    }
    
    func interstitialDidClick() {
    }
    
    func interstitialDidFailToLoadAd() {
        self.performSegue(withIdentifier: "segueMain", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueMain" {
            let _ = segue.destination as! MainViewController
        }
    }
}
