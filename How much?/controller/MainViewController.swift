//
//  MainViewController.swift
//  How much?
//
//  Created by Marcus Ataide on 14/11/16.
//  Copyright © 2016 Math. All rights reserved.
//

import UIKit
import Appodeal

class MainViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var mainBackgroundImage: UIImageView!
    @IBOutlet weak var exchangeTextField: UITextField!
    @IBOutlet weak var btChange: UIButton!
    
    var doubleN:Double?
    
    override func viewDidLoad() {
        self.btChange.layer.cornerRadius = 10.0
        
        // pin ad banner at Bottom of the screen
        
        Appodeal.showAd(AppodealShowStyle.bannerBottom, rootViewController: self)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(MainViewController.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    @IBAction func performExchange(_ sender: Any) {
        
    }
    
     // MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "segueCamera" {
            if self.exchangeTextField.text != "" {
                self.doubleN = Double(self.exchangeTextField.text!)
            }
            if self.doubleN != nil {
                return true
            } else {
                self.exchangeTextField.text = ""
                let alert: UIAlertController  = UIAlertController(title: "Status", message: "Its not a valid number", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return false
            }
        } else {
            return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueCamera" {
            let vc = segue.destination as! CameraViewController
            vc.rateValue = self.doubleN!
            _ = exchangeTextField.resignFirstResponder()
        }
    }
    
    func handleTap(_ tapPressRecognizer: UITapGestureRecognizer) -> Void {
        _ = exchangeTextField.resignFirstResponder()
    }
 
    func animateTextField(_ textField: UITextField, up: Bool) {
        let movementDistance:CGFloat = -130
        let movementDuration: Double = 0.3
        
        var movement:CGFloat = 0
        if up {
            movement = movementDistance
        }
        else {
            movement = -movementDistance
        }
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.animateTextField(textField, up:true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.animateTextField(textField, up:false)
    }
    
}
