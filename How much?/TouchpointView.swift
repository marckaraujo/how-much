//
//  TouchpointView.swift
//  TheFineDetails
//
//  Created by Dave Dombrowski on 12/18/15.
//  Copyright © 2015 justDFD. All rights reserved.
//

import UIKit

class TouchpointView: UIView {
    
    let panGesture = UIPanGestureRecognizer()
    
    init(point: CGPoint, text:String) {
        super.init(frame: CGRect.zero)
        self.frame = CGRect(x: point.x-22, y: point.y-22, width: 44, height: 44)
        self.layer.cornerRadius = 22
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = 2
        self.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.4)
        panGesture.addTarget(self, action: #selector(TouchpointView.move(_:)))
        self.addGestureRecognizer(panGesture)
        let myName = UILabel(frame: CGRect(x: 0, y: 5, width: 44, height: 34))
        //myName.backgroundColor = UIColor.clearColor()
        myName.textColor = UIColor.green
        myName.text = text
        myName.textAlignment = .center
        myName.font = UIFont.boldSystemFont(ofSize: 20)
        self.addSubview(myName)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        let vHair = UIBezierPath()
        vHair.move(to: CGPoint(x: rect.width/2-(1/2), y: 0))
        vHair.addLine(to: CGPoint(x: rect.width/2-(1/2), y: rect.height))
        vHair.addLine(to: CGPoint(x: rect.width/2+(1/2), y: rect.height))
        vHair.addLine(to: CGPoint(x: rect.width/2+(1/2), y: 0))
        vHair.close()
        let hHair = UIBezierPath()
        hHair.move(to: CGPoint(x: 0, y: rect.height/2-(1/2)))
        hHair.addLine(to: CGPoint(x: rect.width, y: rect.height/2-(1/2)))
        hHair.addLine(to: CGPoint(x: rect.width, y: rect.height/2+(1/2)))
        hHair.addLine(to: CGPoint(x: rect.width, y: rect.height/2+(1/2)))
        hHair.close()
        UIColor.red.setStroke()
        vHair.stroke()
        hHair.stroke()
    }
    
    func move(_ recognizer:UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.superview)
        if let view = recognizer.view {
            view.center = CGPoint(x:view.center.x + translation.x,
                y:view.center.y + translation.y)
        }
        recognizer.setTranslation(CGPoint.zero, in: self.superview)
        
        if recognizer.state == UIGestureRecognizerState.ended {
            let velocity = recognizer.velocity(in: self.superview)
            let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y))
            let slideMultiplier = magnitude / 200
            let slideFactor = 0.0 * slideMultiplier     //Increase for more of a slide
            var finalPoint = CGPoint(x:recognizer.view!.center.x + (velocity.x * slideFactor),
                y:recognizer.view!.center.y + (velocity.y * slideFactor))
            finalPoint.x = min(max(finalPoint.x, 0), self.superview!.bounds.size.width)
            finalPoint.y = min(max(finalPoint.y, 0), self.superview!.bounds.size.height)
            UIView.animate(withDuration: Double(slideFactor * 2), delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {recognizer.view!.center = finalPoint }, completion: nil)
        }
    }
}
