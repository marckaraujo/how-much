//
//  CropImageView.swift
//  TheFineDetails
//
//  Created by Dave Dombrowski on 2/11/16.
//  Copyright © 2016 justDFD. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CropImageView: UIImageView {
    
    init() {
        super.init(frame: CGRect.zero)
        translatesAutoresizingMaskIntoConstraints = false
        isUserInteractionEnabled = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        translatesAutoresizingMaskIntoConstraints = false
        isUserInteractionEnabled = true
    }
    
    var scaleFactor:CGFloat {
        if self.image == nil {
            return 0.0
        } else {
            if self.image?.size.width > self.bounds.size.width || self.image?.size.height > self.bounds.size.height {
                // image is scaled down, take the larger of image-bounds, dividing bounds by image
                let widthDiff = (self.image?.size.width)! - self.bounds.size.width
                let heighDiff = (self.image?.size.height)! - self.bounds.size.height
                if widthDiff > heighDiff {
                    return self.bounds.size.width / (self.image?.size.width)!
                } else {
                    return self.bounds.size.height / (self.image?.size.height)!
                }
            } else if self.image?.size.width == self.bounds.size.width || self.image?.size.height == self.bounds.size.height {
                // image is exact fit, return 1
                return 1
            } else {
                // image is scaled up, take the smaller of bounds-image, dividing bounds by image
                let widthDiff = self.bounds.size.width - (self.image?.size.width)!
                let heighDiff = self.bounds.size.height - (self.image?.size.height)!
                if widthDiff < heighDiff {
                    return self.bounds.size.width / (self.image?.size.width)!
                } else {
                    return self.bounds.size.height / (self.image?.size.height)!
                }
            }
        }
    }
    
    var imageSize:CGSize {
        if self.image == nil {
            return CGSize.zero
        } else {
            return CGSize(width: (self.image?.size.width)!, height: (self.image?.size.height)!)
        }
    }
    
    var scaledSize:CGSize {
        if self.image == nil {
            return CGSize.zero
        } else {
            return CGSize(width: (self.image?.size.width)! * self.scaleFactor, height: (self.image?.size.height)! * self.scaleFactor)
        }
    }
    
}
